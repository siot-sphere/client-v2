import { motion } from 'framer-motion';
import { AppRoute } from 'helpers';
import React from 'react';
import { Link } from 'react-router-dom';

export interface LogoProps {
  collapsed: boolean;
}

export default function Logo({ collapsed }: LogoProps) {
  return (
    <motion.div className="text-center w-full">
      <Link to={AppRoute.home}>
        <motion.div
          initial={{ height: '5rem' }}
          animate={{ height: collapsed ? '1rem' : '5rem' }}
          className="w-auto"
        ></motion.div>
      </Link>
    </motion.div>
  );
}
