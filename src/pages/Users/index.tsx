import React, { FC, useState, useEffect } from 'react';
import { Button, Form, Input, Switch, Table, Tooltip, Space, message, Modal, Row } from 'antd';
import { NavLink, useHistory } from 'react-router-dom';
import moment from 'moment';
import styles from './index.module.less';
import { EyeOutlined } from '@ant-design/icons';
import { ColumnsType } from 'antd/lib/table';
import { User } from 'types';
import { AppRoute, useQueryString } from 'helpers';
import { UserService } from 'services';
import { useRecoilState } from 'recoil';
import { currentUser } from 'recoil-stores';
import { ErrorMessage } from 'utils';

interface UserDeactivateFields {
  reason?: string;
}

const UserView: FC = () => {
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState<User[]>([]);
  const [total, setTotal] = useState(0);

  const [current] = useRecoilState(currentUser);
  const histoty = useHistory();

  useEffect(() => {
    if (current && !current.isAdmin) {
      histoty.push('/404');
    }
  }, [current, histoty]);

  const { page = 1, e = '' } = useQueryString();

  useEffect(() => {
    const fetch = async () => {
      setLoading(true);
      try {
        const { body } = await UserService.getAllPaging({ page: +page, limit: 10, email: e });

        setUsers(body[0]);
        setTotal(body[1]);
      } catch (error) {
        ErrorMessage();
      }
      setLoading(false);
    };

    fetch();
  }, [e, page]);

  const onActiveChange = (checked: boolean, record: User) => {
    if (checked) return handleActiveUser(record);
    return handleDeactive(record);
  };

  const activateForm = Form.useForm()[0];

  const handleActiveUser = async (user: User) => {
    try {
      await UserService.deactive({ id: user.id || '', status: !user.isActive, reason: user.reason || '' });

      setUsers([...users.map((u) => (u.id === user.id ? { ...u, isActive: true } : u))]);
    } catch (error) {
      message.error('Oops! Something went wrong!');
    }
  };

  const handleDeactive = async (user: User) => {
    const onFormSubmit = async (close: Function) => {
      await activateForm.validateFields();
      const fields = activateForm.getFieldsValue() as UserDeactivateFields;
      try {
        await UserService.deactive({ id: user.id || '', status: !user.isActive, reason: fields.reason || '' });
      } catch (error) {
        message.error('Oops! Something went wrong!');
      }

      setUsers([...users.map((u) => (u.id === user.id ? { ...u, isActive: false, reason: fields.reason } : u))]);

      close();
    };

    const _renderForm = () => {
      return (
        <Form form={activateForm} className={styles.deactivateForm} layout="vertical">
          <Form.Item
            name="reason"
            label="Reason: "
            rules={[{ required: true, message: 'This field is required', whitespace: true }, {}]}
          >
            <Input type="textarea" placeholder="Deactive reason" />
          </Form.Item>
        </Form>
      );
    };
    Modal.confirm({
      title: 'Deactive reason',
      onOk: onFormSubmit,
      content: _renderForm(),
    });
  };

  const [user] = useRecoilState(currentUser);

  const columns: ColumnsType<User> = [
    {
      title: '',
      dataIndex: 'no',
      width: 70,
      render: (text, record, index) => <span>{index + 1}</span>,
    },
    {
      title: 'Email',
      dataIndex: 'email',
    },
    {
      title: 'First name',
      dataIndex: 'firstName',
    },
    {
      title: 'Last name',
      dataIndex: 'lastName',
    },
    {
      title: 'Roles',
      sorter: (p, n) => (p.isAdmin ? 1 : 0) - (n.isAdmin ? 1 : 0),
      render: (text, record) => (record.isAdmin ? 'Admin' : 'User'),
    },
    {
      title: 'Joined date',
      dataIndex: 'created_at',
      render: (text, record) => <span>{record?.createdAt ? moment(record.createdAt).format('DD/MM/yyyy') : ''}</span>,
    },
    {
      title: 'Number of devices',
      dataIndex: 'devices',
      sorter: (prev, cur) => prev.devices.length - cur.devices.length,
      render: (text, record) => <span>{record.devices.length}</span>,
    },
    {
      title: 'Status',
      dataIndex: 'status',
      sorter: (p, n) => (p.isActive ? 1 : 0) - (n.isActive ? 1 : 0),
      render: (text, record) => (
        <Switch
          disabled={user?.id === record.id}
          checked={record.isActive}
          onChange={(checked) => onActiveChange(checked, record)}
        />
      ),
    },
    {
      title: 'Actions',
      dataIndex: 'action',
      width: 100,
      render: (text, record) => (
        <Space>
          <Tooltip title="View detail">
            <NavLink
              to={{
                pathname: AppRoute.user_detail_ref(record.id || ''),
              }}
            >
              <Button icon={<EyeOutlined />} />
            </NavLink>
          </Tooltip>
        </Space>
      ),
    },
  ];

  const [form] = Form.useForm();

  return (
    <Form form={form} initialValues={{ email: e }}>
      <Row justify="space-between">
        <Form.Item name="email" noStyle>
          <Input placeholder="Filter by email" style={{ width: 400 }} />
        </Form.Item>
        <Button onClick={() => histoty.push(AppRoute.users + '?e=' + (form.getFieldValue('email') || ''))}>
          Filter
        </Button>
      </Row>
      <div className={styles.table}>
        <Table
          loading={loading}
          rowKey={(record) => record?.id || ''}
          dataSource={users}
          columns={columns}
          pagination={{
            current: 1,
            pageSize: 10,
            total,
            position: ['bottomCenter'],
            showSizeChanger: false,
          }}
        />
      </div>
    </Form>
  );
};

export default UserView;
