import React from 'react';
import { HomeOutlined } from '@ant-design/icons';
import { AppRoute } from 'helpers';

export default {
  route: {
    routes: [
      {
        path: AppRoute.home,
        icon: <HomeOutlined />,
        name: 'My devices',
      },
    ],
  },
};
