import superagent from 'superagent';
import SuperagentPrefix from 'superagent-prefix';
import { User } from 'types';

const prefix = SuperagentPrefix(process.env.REACT_APP_API_ENDPOINT + '/api/users');

const getAllPaging = ({ page, limit, email }: { page: number; limit: number; email: string }) => {
  const token = localStorage.getItem('accessToken');

  return superagent
    .get(`/?page=${page}&limit=${limit}&email=${email}`)
    .use(prefix)
    .set('Authorization', 'Bearer ' + token) as unknown as Promise<{ body: [User[], number] }>;
};

const getById = (id: string) => {
  const token = localStorage.getItem('accessToken');

  return superagent
    .get('/' + id)
    .use(prefix)
    .set('Authorization', 'Bearer ' + token) as unknown as Promise<{ body: User }>;
};

const deactive = ({ id, status, reason }: { id: string; status: boolean; reason: string }) => {
  const token = localStorage.getItem('accessToken');

  return superagent
    .post('/' + id + '/deactive')
    .use(prefix)
    .set('Authorization', 'Bearer ' + token)
    .send({ status, reason }) as unknown;
};

export default { getAllPaging, getById, deactive };
