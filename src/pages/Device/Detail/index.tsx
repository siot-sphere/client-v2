import React, { useState, useEffect } from 'react';
import { Tabs, Row, Col, Divider, Modal, Tooltip } from 'antd';
import { Color, ErrorMessage } from 'utils';
import { Device } from 'types';
import { Link, useHistory, useParams } from 'react-router-dom';
import { DeviceService } from 'services';
import DeviceName from './deviceName';
import AttributeTab from './Attribute';
import DeviceAvatar from './deviceAvatar';
import { useQueryString } from 'hooks';
import { useRecoilState } from 'recoil';
import { currentUser } from 'recoil-stores';
import Button from 'antd/es/button/button';
import { AppRoute } from 'helpers';
import ToggleAutoUpdatePlatformVersion from './ToggleAutoUpdatePlatformVersion';

const { TabPane } = Tabs;

export interface DeviceDetailProps {}

const DeviceDetail: React.SFC<DeviceDetailProps> = () => {
  const [device, setDevice] = useState<Device | undefined>();

  const [current] = useRecoilState(currentUser);
  const histoty = useHistory();

  useEffect(() => {
    if (current && device && !current.isAdmin && current.id !== device.userId) {
      histoty.push('/404');
    }
  }, [current, histoty, device]);

  const { slug } = useParams<{ slug: string }>();

  useEffect(() => {
    const fetch = async () => {
      try {
        const { body } = await DeviceService.getDeviceBySlugName(slug);

        setDevice(body);
      } catch (e) {
        ErrorMessage();
      }
    };

    if (slug) {
      fetch();
    }
  }, [slug]);

  const handleDeleteDevice = () => {
    Modal.confirm({
      title: 'Delete ' + device?.name,
      onOk: async () => {
        try {
          await DeviceService.deleteDevice(slug);
          histoty.push(AppRoute.home);
          window.location.reload();
        } catch (error) {
          ErrorMessage();
        }
      },
      content: 'Are your sure you want to delete this device?',
    });
  };

  const { tab = 1 } = useQueryString();

  return (
    <div className="device-container">
      <div className="device_header">
        <h2 className="h2">{current?.isAdmin ? device?.name : 'Settings'}</h2>
      </div>
      <div className="device_tabs">
        <Tabs
          defaultActiveKey={`${tab}`}
          onChange={() => {}}
          tabBarStyle={{ borderBottom: '1px solid ' + Color.PRIMARY }}
        >
          <TabPane tab="General" key="1">
            <Row justify="center">
              <Col className="device_tabs_content">
                <div className="device_tabs_content-lable">Your device</div>
                <div className="device_tabs_content-card">
                  <DeviceName device={device} onDeviceChange={setDevice} disable={current?.id !== device?.userId} />
                  <Row className="card-option" align="middle">
                    <Col className="card-label" span={6}>
                      Device ID
                    </Col>
                    <Col>
                      <span className="active">{device?.id}</span>
                    </Col>
                  </Row>
                  <Row className="card-option" align="middle">
                    <Col className="card-label" span={6}>
                      Slug API key
                    </Col>
                    <Col>
                      <span className="active">{device?.slug}</span>
                    </Col>
                  </Row>
                  <Row className="card-option" align="middle" style={{ flexWrap: 'nowrap' }}>
                    <Col className="card-label" span={6}>
                      Bearer token
                    </Col>
                    <Col>
                      <span className="active" style={{ wordBreak: 'break-word' }}>
                        {localStorage.getItem('deviceSignToken')}
                      </span>
                    </Col>
                  </Row>
                  <Row className="card-option" align="middle" style={{ flexWrap: 'nowrap' }}>
                    <Col className="card-label" span={6}>
                      Current Platform Version
                    </Col>
                    <Col>
                      <span className={`active ${device?.platformVersion?.name ? 'text-blue' : 'text-primary'}`}>
                        {device?.platformVersion?.name || 'Unknow'}
                      </span>
                      <Link to={AppRoute.versions}>
                        <span className="ml-8">View release list</span>
                      </Link>
                    </Col>
                  </Row>
                  <Row className="card-option" align="middle" style={{ flexWrap: 'nowrap' }}>
                    <Col className="card-label" span={6}>
                      Current Platform Version Key
                    </Col>
                    <Col>
                      <span className={`active ${device?.platformVersion?.name ? 'text-blue' : 'text-primary'}`}>
                        {device?.platformVersion?.releaseKey || 'Unknow'}
                      </span>
                    </Col>
                  </Row>
                  <Row className="card-option" align="middle" style={{ flexWrap: 'nowrap' }}>
                    <Col className="card-label" span={6}>
                      Allow auto Update
                    </Col>
                    <Col>
                      <span className="active" style={{ wordBreak: 'break-word' }}>
                        <Tooltip title="Toggle device auto update">
                          <ToggleAutoUpdatePlatformVersion device={device} />
                        </Tooltip>
                      </span>
                      <div>
                        <span className="text-sm italic">
                          (This feature is only available when the Platform Version is installed on embedded device)
                        </span>
                      </div>
                    </Col>
                  </Row>
                  <Row className="card-option" align="middle">
                    <Col className="card-label" span={6}>
                      Device image
                    </Col>
                    <Col>{device && <DeviceAvatar device={device} />}</Col>
                  </Row>
                </div>
              </Col>
            </Row>
            <Row justify="center" style={{ marginTop: 24 }}>
              <Col className="device_tabs_content">
                <div className="device_tabs_content-lable">Attribute API</div>
                <div className="device_tabs_content-card">
                  {(device?.attributes || []).map((attr, index) => (
                    <>
                      <Row className="card-option" align="middle" key={attr.id}>
                        <Col className="card-label" span={24} style={{ marginBottom: 6 }}>
                          Attribute: <strong>{attr.name}</strong>
                        </Col>
                        <Col span={24}>
                          <Row className="card-option flex-nowrap" align="middle">
                            <Col span={6}>
                              <strong>Rest method:</strong>
                            </Col>
                            <Col span={18} className="p-2 bg-black-8">
                              Post
                            </Col>
                          </Row>

                          <Row className="card-option flex-nowrap" align="middle">
                            <Col span={6}>
                              <strong>HTTP Endpoint:</strong>
                            </Col>
                            <Col span={18} className="p-2 bg-black-8">
                              {process.env.REACT_APP_API_ENDPOINT +
                                '/api/devices/' +
                                device?.slug +
                                '/attributes/' +
                                attr.slug}
                            </Col>
                          </Row>

                          <Row className="card-option flex-nowrap" align="middle">
                            <Col span={6}>
                              <strong>HTTP Header Format:</strong>
                            </Col>
                            <Col span={18} className="p-2 bg-black-8">
                              <div>
                                {`'Platform-Version': {Current Platform Version Key}`}{' '}
                                <span className="text-sm italic">(Optional)</span>
                              </div>

                              <div>{`'Content-Type': application/json`}</div>
                              <div>{`'Authorization': Bearer {Bearer token}`}</div>
                            </Col>
                          </Row>

                          <Row className="card-option flex-nowrap" align="middle">
                            <Col span={6}>
                              <strong>HTTP Body Format:</strong>
                            </Col>
                            <Col span={18} className="p-2 bg-black-8">
                              <div>{`{`}</div>
                              <div className="ml-4">{`value={Attribute value}`}</div>
                              <div>{`}`}</div>
                            </Col>
                          </Row>

                          <Row className="card-option flex-nowrap" align="middle">
                            <Col span={6}>
                              <strong>Value type:</strong>
                            </Col>
                            <Col span={18} className="p-2 bg-black-8">
                              {attr.dataType.toLowerCase()}
                            </Col>
                          </Row>
                        </Col>
                      </Row>
                      {index !== (device?.attributes?.length || 1) - 1 && <Divider type="horizontal" />}
                    </>
                  ))}
                </div>
              </Col>
            </Row>
            <Row justify="center">
              <Button type="link" style={{ color: Color.PINK }} onClick={handleDeleteDevice}>
                Delete {device?.name}
              </Button>
            </Row>
          </TabPane>
          <TabPane tab="Attributes" key="attributes">
            <AttributeTab attributes={device?.attributes || []} device={device} />
          </TabPane>
        </Tabs>
      </div>
    </div>
  );
};

export default DeviceDetail;
