import React from 'react';

const Blank: React.SFC = React.memo(({ children }) => {
  return (
    <div>
      <div>{children}</div>
    </div>
  );
});

export default Blank;
