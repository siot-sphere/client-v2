import { Col, Form, Input, Row, Typography } from 'antd';
import Button from 'antd/es/button/button';
import { Header } from 'components';
import { AppRoute, useValidationMessage } from 'helpers';
import React, { useState } from 'react';
import { AuthService } from 'services';
import { User } from 'types';
import PassowrdConfirmation from './PasswordConfirm';
import { Color } from 'utils';
import { useRecoilState } from 'recoil';
import { authenticated, currentUser } from 'recoil-stores';
import { useHistory } from 'react-router-dom';
import { Store } from 'antd/lib/form/interface';

export interface SignupProps {}

const Signup: React.SFC<SignupProps> = () => {
  const [message, setMessage] = useState<string | undefined>();
  const [loading, setLoading] = useState(false);

  const [, setCurrentUser] = useRecoilState(currentUser);
  const [, setLogin] = useRecoilState(authenticated);

  const history = useHistory();

  const onFinish = async (store: Store) => {
    setLoading(true);

    try {
      const {
        body: { accessToken, user, deviceSignToken },
      } = await AuthService.register(store as User);

      localStorage.setItem('accessToken', accessToken);
      localStorage.setItem('deviceSignToken', deviceSignToken);
      setCurrentUser(user);
      setLogin({ isLogin: true });

      history.push(AppRoute.home);
      window.location.reload();
    } catch (error) {
      const ERROR = JSON.parse(JSON.stringify(error));

      if (ERROR && ERROR.response && ERROR.response.body && ERROR.response.body.errno === 1062) {
        setMessage('Your email has already taken!');
      } else {
        setMessage('Oops! Something went wrong!');
      }
    }

    setLoading(false);
  };

  const validateMessages = useValidationMessage();

  return (
    <Row justify="center" align="middle" style={{ minHeight: '100vh' }}>
      <Col span={6}>
        <Header
          position="center"
          title={
            <h1 className="h1" style={{ textTransform: 'uppercase', marginBottom: 24 }}>
              Signup
            </h1>
          }
        />
        <Form name="normal_login" className="login-form" onFinish={onFinish} validateMessages={validateMessages}>
          <Form.Item name="email" rules={[{ required: true }, { type: 'email' }]}>
            <Input placeholder="Email" />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{ required: true }, { min: 6, message: 'Password must be at least 6 characters' }]}
          >
            <Input type="password" placeholder="Password" />
          </Form.Item>

          <PassowrdConfirmation />

          <Form.Item name="firstName" rules={[{ required: true }]}>
            <Input placeholder="First Name" />
          </Form.Item>

          <Form.Item name="lastName" rules={[{ required: true }]}>
            <Input placeholder="Last Name" />
          </Form.Item>

          {message && (
            <Form.Item noStyle>
              <Typography style={{ color: Color.HOT }}>{message}</Typography>
            </Form.Item>
          )}

          <Form.Item noStyle>
            <Button type="primary" htmlType="submit" block loading={loading} disabled={loading}>
              Signup
            </Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};

export default Signup;
