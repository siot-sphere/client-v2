import superagent from 'superagent';
import SuperagentPrefix from 'superagent-prefix';
import { User } from 'types';

const prefix = SuperagentPrefix(process.env.REACT_APP_API_ENDPOINT + '/api/auth');

async function login(email: string, password: string, deviceToken: string) {
  return superagent.post('/login').use(prefix).send({ email, password, deviceToken }) as unknown as Promise<{
    body: { accessToken: string; user: User; deviceSignToken: string };
  }>;
}

async function checkMe(token: string) {
  return superagent.get('/me').use(prefix).set('Authorization', `Bearer ${token}`) as unknown as Promise<{
    body: User;
  }>;
}

async function register(user: User) {
  return superagent
    .post('/register')
    .use(prefix)
    .send({ ...user }) as unknown as Promise<{
    body: { accessToken: string; user: User; deviceSignToken: string };
  }>;
}

async function logout() {
  const token = localStorage.getItem('accessToken');

  if (!token) {
    return null;
  }

  return superagent.post('/logout').use(prefix).set('Authorization', `Bearer ${token}`);
}

export default { login, checkMe, register, logout };
