export type User = {
  id?: string;
  firstName: string;
  lastName: string;
  isAdmin?: boolean;
  isActive?: boolean;
  email: string;
  reason?: string;
  password: string;
  createdAt?: string;
  updatedAt?: string;
  devices: Device[];
};

export type Device = {
  id?: string;
  name: string;
  slug: string;
  image: string;
  isActive?: boolean;
  createdAt?: string;
  updatedAt?: string;
  user?: User;
  userId?: string;
  attributes?: Attribute[];
  platformVersion?: PlatformVersionEntity;
  allowAutoUpdate?: boolean;
  currentPlatformVersionId?: number;
};

export type Attribute = {
  id?: number;
  name: string;
  slug: string;
  dataType: AttributeDataType;
  dataLabel: string;
  isActive?: boolean;
  createdAt?: string;
  updatedAt?: string;
  device?: Device;
  deviceId?: string;
  lastActive?: string;
  totalRecord?: number;
  min?: number;
  max?: number;
};

export type AttributeValue = {
  id?: number;
  value: AttributeDataType;
  attributeId: number;
  createdAt: string;
  updatedAt: string;
  count: number;
};

export enum AttributeDataType {
  FLOAT = 'FLOAT',
  INTEGER = 'INTEGER',
  STRING = 'STRING',
  BIT = 'BIT',
}

export interface DeviceEditInput {
  name?: string;
  image?: string;
  allowAutoUpdate?: boolean;
}

export enum PeriodEnum {
  '1_DAY' = '1_DAY',
  '3_DAY' = '3_DAY',
  '1_WEEK' = '1_WEEK',
  '1_MONTH' = '1_MONTH',
  '3_MONTH' = '3_MONTH',
  '6_MONTH' = '6_MONTH',
}

export interface Banner {
  id: number;
  url: string;
}

export type PlatformVersionEntity = {
  id?: number;
  name: string;
  releaseKey: string;
  description: string;
  releaseType: PlatformVersionReleaseEnum;
  medias?: PlatformMediaEntity[];
  devices?: Device[];
  createdAt?: string;
  updatedAt?: string;
  user?: User;
};

export enum PlatformVersionReleaseEnum {
  STABLE = 'STABLE',
  BETA = 'BETA',
}

export type PlatformMediaEntity = {
  id?: number;
  fileName: string;
  filePath: string;
  createdAt?: string;
  updatedAt?: string;
  platformVersion?: PlatformMediaEntity;
  versionId?: number;
};

export type QueryPagination = {
  page: number;
  limit: number;
};
