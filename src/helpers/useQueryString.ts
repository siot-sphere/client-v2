import { useLocation } from 'react-router-dom';
import qs from 'qs';

export default function useQueryString() {
  const location = useLocation();

  return qs.parse(location.search, { ignoreQueryPrefix: true }) as unknown as { [key: string]: string };
}
