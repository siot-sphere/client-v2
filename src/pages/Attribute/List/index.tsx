import React, { useMemo, useState } from 'react';
import { Col, Form, message, Row, Table } from 'antd';
import { useParams } from 'react-router-dom';
import moment from 'moment';
import styles from './index.module.less';
import { ColumnsType } from 'antd/lib/table';
import { FormProps } from 'antd/lib/form';
import { AttributeValue, Attribute } from 'types';
import { DeviceService } from 'services';
import { useNavigate } from 'hooks';
import DateTimePicker from '../DateTimePicker';

interface UserListUrlParams {
  fromDate?: string;
  toDate?: string;
  page?: string;
  name?: string;
  role?: string;
}

interface UserFields {
  search?: string;
}

const List = ({ attribute }: { attribute: Attribute }) => {
  const [loading, setLoading] = useState(false);
  const [values, setValues] = useState<AttributeValue[]>([]);
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [date, setDate] = useState<any>();

  const { deviceSlug, attributeSlug } = useParams<{ deviceSlug: string; attributeSlug: string }>();

  const [form] = Form.useForm();

  const handleDateTimeChange = async ([from, to]: [moment.Moment, moment.Moment]) => {
    setLoading(true);
    setDate([from, to]);
    try {
      const {
        body: [value, total],
      } = await DeviceService.getAttributeValuePaging(deviceSlug, attributeSlug, {
        page: +page,
        from: from.toISOString(),
        to: to.toISOString(),
      });

      setValues(value);
      setTotal(total);
    } catch (error) {
      message.error('Oops! Something went wrong!');
    }

    setLoading(false);
  };

  const handlePageChange = async (page: number) => {
    setLoading(true);
    setPage(page);
    try {
      const {
        body: [value, total],
      } = await DeviceService.getAttributeValuePaging(deviceSlug, attributeSlug, {
        page: +page,
        from: date[0].toISOString(),
        to: date[1].toISOString(),
      });

      setValues(value);
      setTotal(total);
    } catch (error) {
      message.error('Oops! Something went wrong!');
    }

    setLoading(false);
  };

  const formLayout: FormProps = {
    labelCol: {
      span: 4,
    },
    wrapperCol: { span: 16 },
  };

  const columns: ColumnsType<AttributeValue> = [
    {
      title: '',
      dataIndex: 'no',
      width: 70,
      render: (text, record, index) => <span>{index + 1}</span>,
    },
    {
      title: `Value (${attribute.dataLabel})`,
      dataIndex: 'value',
    },
    {
      title: 'Created date',
      dataIndex: 'created_at',
      render: (text, record) => (
        <span>{record?.createdAt ? moment(record.createdAt).format('DD/MM/YYYY HH:mm:ss') : ''}</span>
      ),
    },
  ];

  const navigate = useNavigate<UserListUrlParams>();

  const onFormFinish = () => {
    const { date, role, search } = form.getFieldsValue();

    navigate({
      role,
      name: search,
      fromDate: date && date[0] ? new Date(date[0]['_d']).toISOString() : undefined,
      toDate: date && date[1] ? new Date(date[1]['_d']).toISOString() : undefined,
    });
  };

  const onFormReset = () => {};

  const formInitialValues: UserFields = useMemo(() => {
    return {};
  }, []);

  return (
    <Form form={form} initialValues={formInitialValues} onFinish={onFormFinish} onReset={onFormReset} {...formLayout}>
      <div style={{ marginTop: 12 }}>
        <Form.Item label="Name" noStyle>
          <div>Name: {attribute.name}</div>
        </Form.Item>
        <Form.Item label="Created at" noStyle>
          <div>Created at: {moment(attribute.createdAt || '').format('DD/MM/YYYY HH:mm:ss')}</div>
        </Form.Item>
        <Row>
          <Col span={4}>
            <Form.Item label="Max value" labelCol={{ md: 12 }} noStyle>
              Max value: {attribute?.max || 0} {attribute?.dataLabel}
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item label="Min value" noStyle>
              Min value: {attribute?.min || 0} {attribute?.dataLabel}
            </Form.Item>
          </Col>
        </Row>
        <Form.Item label="Last active" noStyle>
          <div>Last active: {moment(attribute?.lastActive).format('DD/MM/YYYY HH:mm:ss')}</div>
        </Form.Item>
      </div>

      <div style={{ marginTop: 12 }}>
        <DateTimePicker onDateTimeChange={handleDateTimeChange} />
      </div>

      <div className={styles.table}>
        <div>
          Total: {total || 0} in {attribute?.totalRecord || 0} total records
        </div>
        <Table
          loading={loading}
          rowKey={(record) => record?.id || ''}
          dataSource={values}
          columns={columns}
          pagination={{
            current: +page,
            pageSize: 10,
            total: total,
            onChange: (page) => handlePageChange(page),
            position: ['bottomCenter'],
            showSizeChanger: false,
          }}
        />
      </div>
    </Form>
  );
};

export default List;
