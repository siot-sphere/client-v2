module.exports = {
  purge: ['./public/**/*.html', './src/**/*.{js,jsx,ts,tsx,vue}'],
  mode: 'jit',
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      green: {
        DEFAULT: '#34A853',
        8: 'rgba(52, 168, 83, 0.08)',
      },
      primary: {
        DEFAULT: '#F04D56',
        8: 'rgba(240, 77, 86, 0.08)',
      },
      orange: {
        DEFAULT: '#FF8310',
        8: 'rgba(255, 131, 16, 0.08)',
      },
      blue: {
        DEFAULT: '#4285F4',
        8: 'rgba(66, 133, 244, 0.08)',
      },
      white: '#FFFFFF',
      black: {
        DEFAULT: '#000000',
        65: 'rgba(0, 0, 0, 0.65)',
        35: 'rgba(0, 0, 0, 0.35)',
        8: 'rgba(0, 0, 0, 0.08)',
        3: 'rgba(0, 0, 0, 0.03)',
      },
      transparent: 'transparent',
    },
    borderRadius: {
      none: '0',
      xs: '1px',
      sm: '2px',
      md: '4px',
      DEFAULT: '8px',
      lg: '12px',
      full: '9999px',
    },
    boxShadow: {
      DEFAULT: '0px 4px 30px rgba(0, 0, 0, 0.02)',
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
  important: true,
};
