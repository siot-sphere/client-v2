import React from 'react';

import { RootRouter } from './RouterContainer';
import { BrowserRouter as Router } from 'react-router-dom';
import { RecoilRoot } from 'recoil';
import firebase from 'firebase';
import { ConfigProvider as AntdFormConfigProvider } from 'antd';

import { firebaseConfig } from 'helpers';
import { registerServiceWorker } from 'serviceWorker';

import { validateMessages } from './utils';
import { QueryClient, QueryClientProvider } from 'react-query';

import 'antd/dist/antd.less';
import './App.css';
import './styles/main.less';

firebase.initializeApp(firebaseConfig);

registerServiceWorker();

if (firebase.messaging.isSupported()) {
  firebase
    .messaging()
    .requestPermission()
    .catch(() => {});
}

const queryClient = new QueryClient({
  defaultOptions: {
    mutations: {
      retryDelay: 0,
    },
    queries: {
      cacheTime: 0,
    },
  },
});

function App() {
  return (
    <RecoilRoot>
      <QueryClientProvider client={queryClient}>
        <AntdFormConfigProvider form={{ validateMessages }}>
          <Router>
            <RootRouter />
          </Router>
        </AntdFormConfigProvider>
      </QueryClientProvider>
    </RecoilRoot>
  );
}

export default App;
