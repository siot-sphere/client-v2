import { Button, Popover, Typography } from 'antd';
import { motion } from 'framer-motion';
import appRoute from 'helpers/app.route';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { Color } from 'utils';

export interface HeaderProps {}

export default function Header(props: HeaderProps) {
  const history = useHistory();

  return (
    <div className="h-full w-full leading-normal">
      <div className="flex justify-end items-center h-full">
        <motion.div whileHover={{ color: Color.PRIMARY }} className="cursor-pointer w-7 h-7"></motion.div>
        <motion.div whileHover={{ color: Color.PRIMARY }} className="cursor-pointer w-7 h-7 mx-8"></motion.div>
        <Popover
          content={
            <Button type="link" onClick={() => history.push(appRoute.logout)}>
              Log out
            </Button>
          }
        >
          <motion.div whileHover={{ color: Color.PRIMARY }} className="flex items-center cursor-pointer h-10 group">
            <div className="w-7 h-7 border-1 rounded-full border-blue-8 shadow group-hover:border-primary-65"></div>
            <div className="mx-2">
              <Typography.Text className="font-medium text-inherit"></Typography.Text>
            </div>
            <div className="w-4 h-4"></div>
          </motion.div>
        </Popover>
      </div>
    </div>
  );
}
