import superagent from 'superagent';
import SuperagentPrefix from 'superagent-prefix';
import { Banner } from 'types';

const prefix = SuperagentPrefix(process.env.REACT_APP_API_ENDPOINT + '/api/banners');

async function save(url: string) {
  const token = localStorage.getItem('accessToken');

  return superagent.post('/').use(prefix).set('Authorization', `Bearer ${token}`).send({ url }) as unknown as Promise<{
    body: Banner;
  }>;
}

async function deleteBanner(id: number) {
  const token = localStorage.getItem('accessToken');
  return superagent
    .delete('/' + id)
    .use(prefix)
    .set('Authorization', `Bearer ${token}`);
}

async function getAllPaging(page: number, limit = 20) {
  const token = localStorage.getItem('accessToken');
  return superagent.get('/').use(prefix).set('Authorization', `Bearer ${token}`).query({
    page,
    limit,
  }) as Promise<{ body: Banner[] }>;
}

export default { save, deleteBanner, getAllPaging };
