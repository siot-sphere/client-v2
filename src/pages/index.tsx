import { Spinner } from 'components';
import React, { lazy, LazyExoticComponent, Suspense } from 'react';

const Page = ({ lazy: LazyPage, ...props }: { lazy: LazyExoticComponent<any> }) => {
  return (
    <Suspense fallback={<Spinner loading={true} />}>
      <LazyPage {...props} />
    </Suspense>
  );
};

export const HomePage = () => <Page lazy={lazy(() => import('./Home'))} />;
export const LoginPage = () => <Page lazy={lazy(() => import('./Login'))} />;
export const PageError404 = () => <Page lazy={lazy(() => import('./Errors/404'))} />;
export const PageError500 = () => <Page lazy={lazy(() => import('./Errors/500'))} />;
export const LogoutPage = () => <Page lazy={lazy(() => import('./Logout'))} />;
export const SignupPage = () => <Page lazy={lazy(() => import('./Signup'))} />;
export const DeviceFormPage = () => <Page lazy={lazy(() => import('./Device/New'))} />;
export const DeviceInstructionPage = () => <Page lazy={lazy(() => import('./Device/Instruction'))} />;
export const DeviceDetailPage = () => <Page lazy={lazy(() => import('./Device/Detail'))} />;
export const AttributePage = () => <Page lazy={lazy(() => import('./Attribute'))} />;
export const Users = () => <Page lazy={lazy(() => import('./Users'))} />;
export const UserDetail = () => <Page lazy={lazy(() => import('./Users/detail'))} />;
export const AttributeEditPage = () => <Page lazy={lazy(() => import('./AttributeEdit'))} />;
export const AttributeNewPage = () => <Page lazy={lazy(() => import('./AttributeNew'))} />;
export const BannerPage = () => <Page lazy={lazy(() => import('./Banner'))} />;
export const VersionManagementPage = () => <Page lazy={lazy(() => import('./VersionManagement'))} />;
export const NewVersionPage = () => <Page lazy={lazy(() => import('./NewVersion'))} />;
export const EditVerionPage = () => <Page lazy={lazy(() => import('./EditVersion'))} />;
export const ReleaseList = () => <Page lazy={lazy(() => import('./ReleaseList'))} />;
export const ApiDoc = () => <Page lazy={lazy(() => import('./API'))} />;
