import { Col, Row } from 'antd';
import React, { ReactNode } from 'react';

export interface HeaderProps {
  position?: 'start' | 'end' | 'center';
  title: ReactNode;
}

const Header: React.SFC<HeaderProps> = ({ position = 'start', title }) => {
  return (
    <Col span={24}>
      <Row justify={position}>{title}</Row>
    </Col>
  );
};

export default Header;
