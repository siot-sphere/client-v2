import React, { memo, useState } from 'react';
import { Form, Input, Button, Row, Col, message as messageNoti } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { AuthService, DeviceService } from 'services';
import { useRecoilState } from 'recoil';
import { authenticated, currentUser, deviceList } from 'recoil-stores';
import { AppRoute, useQueryString } from 'helpers';
import { Link, useHistory } from 'react-router-dom';
import { Header } from 'components';
import { getDeviceToken } from 'helpers/firebase';

const LoginContainer: React.SFC = memo(() => {
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [message, setErrorMessage] = useState();

  const [, setLogin] = useRecoilState(authenticated);
  const [, setUser] = useRecoilState(currentUser);
  const [, setDevices] = useRecoilState(deviceList);

  const form = Form.useForm()[0];

  const { redirect } = useQueryString();
  const history = useHistory();

  const onFinish = async (store: any) => {
    setLoading(true);
    const { email, password } = store;
    try {
      const deviceToken = await getDeviceToken();

      const {
        body: { accessToken, deviceSignToken },
      } = await AuthService.login(email, password, deviceToken);

      const { body: user } = await AuthService.checkMe(accessToken);

      setLogin({ isLogin: true });
      setUser(user);

      localStorage.setItem('accessToken', accessToken);
      localStorage.setItem('deviceSignToken', deviceSignToken);

      const { body: devices } = await DeviceService.getDeviceByUser();

      setDevices(devices);

      if (redirect) {
        history.push(redirect);
      } else {
        history.push(AppRoute.home);
      }

      window.location.reload();
    } catch (error) {
      messageNoti.error('Oops! Something went wrong!');

      if (error?.response?.body?.reason) {
        setErrorMessage(error?.response?.body?.reason);
      }

      setError(true);
    }
    setLoading(false);
  };

  const handleFieldsChange = () => {
    setError(false);
  };

  return (
    <Row justify="center" align="middle" style={{ minHeight: '100vh' }}>
      <Col span={6}>
        <Header
          position="center"
          title={
            <h1 className="h1" style={{ textTransform: 'uppercase', marginBottom: 24 }}>
              Login
            </h1>
          }
        />
        <Form
          name="normal_login"
          className="login-form"
          onFinish={onFinish}
          form={form}
          onFieldsChange={handleFieldsChange}
        >
          <Form.Item name="email" rules={[{ required: true, message: 'Please input your email!' }]}>
            <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
          </Form.Item>
          <Form.Item name="password" rules={[{ required: true, message: 'Please input your Password!' }]}>
            <Input prefix={<LockOutlined className="site-form-item-icon" />} type="password" placeholder="Password" />
          </Form.Item>

          <Form.Item
            className="form-message"
            help={
              message
                ? 'Your account have been deactive. Reason: ' + message
                : error
                ? `Email or password incorrect`
                : undefined
            }
            validateStatus={error ? 'error' : undefined}
          />
          {message && (
            <div>
              Need support? Mail to <a href="mailto:admin@ms.hust.edu.vn">SIoT Admin</a>.
            </div>
          )}
          <Form.Item noStyle>
            <Button type="primary" htmlType="submit" block loading={loading} disabled={loading}>
              Login
            </Button>{' '}
          </Form.Item>
          <Form.Item>
            {`Don't have account?`} <Link to={AppRoute.signup}>register now!</Link>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
});

export default LoginContainer;
