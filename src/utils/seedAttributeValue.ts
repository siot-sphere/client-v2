import { DeviceService } from 'services';
import { useEffect } from 'react';
import { useRecoilState } from 'recoil';
import { deviceList } from 'recoil-stores';

function getRandomInt(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export default () => {
  const [devices] = useRecoilState(deviceList);

  useEffect(() => {
    const otto = devices.find((d) => d.slug === 'ottodiy');

    if (!otto) {
      return;
    }

    const insert = setInterval(() => {
      const c = getRandomInt(0, otto?.attributes?.length! - 1);
      const value = getRandomInt(22, 30);

      DeviceService.insert(otto?.slug!, otto?.attributes![c]!.slug!, value);
    }, 5000);

    return () => {
      clearInterval(insert);
    };
  });

  return null;
};
