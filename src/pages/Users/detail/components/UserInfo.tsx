import React, { FC, memo } from 'react';
import { Col, Row } from 'antd';
import { User } from 'types';

import styles from './index.module.less';

interface Props {
  user: User;
}
const UserInfo: FC<Props> = (props) => {
  const { user } = props;

  return (
    <div className={styles.box}>
      <div className={styles.title}>User profile</div>
      <div className={styles.content}>
        <Row gutter={[8, 32]}>
          <Col span={8}>
            <span className={styles.label}>Email</span>
          </Col>
          <Col flex={1}>
            <span>{user?.email}</span>
          </Col>
        </Row>
        <Row gutter={[8, 32]}>
          <Col span={8}>
            <span className={styles.label}>Full name</span>
          </Col>
          <Col flex={1}>
            <span>{user.firstName + ' ' + user.lastName}</span>
          </Col>
        </Row>
        <Row gutter={[8, 32]}>
          <Col span={8}>
            <span className={styles.label}>Status</span>
          </Col>
          <Col flex={1}>
            <span style={{ color: user.isActive ? '#004684' : undefined }}>
              {user.isActive ? 'Active' : 'Unactive'}
            </span>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default memo(UserInfo);
