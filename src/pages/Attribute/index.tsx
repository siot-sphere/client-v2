import { Spinner } from 'components';
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { DeviceService } from 'services';
import { Attribute as AttributeEntity, AttributeDataType } from 'types';
import { useRecoilState } from 'recoil';
import { currentUser, deviceList } from 'recoil-stores';
import { Row, Col, Divider, message } from 'antd';
import RealtimeChart from './RealtimeChart';
import List from './List';
import FrequentiyChart from './FrequentlyChart';
import ImportDataModal from './ImportDataModal';
import OldDataChart from './OldDataChart';

export interface AttributeProps {}

const Attribute: React.SFC<AttributeProps> = () => {
  const [current] = useRecoilState(currentUser);
  const [attribute, setAttribute] = useState<AttributeEntity>();

  const { deviceSlug, attributeSlug } = useParams<{ deviceSlug: string; attributeSlug: string }>();

  const [devices] = useRecoilState(deviceList);

  useEffect(() => {
    const fetch = async () => {
      try {
        const { body: attributeDetail } = await DeviceService.getAttributeDetail(attributeSlug);

        setAttribute(attributeDetail);
      } catch (error) {
        message.error('Oops! Something went wrong!');
      }
    };

    if (attributeSlug && deviceSlug) {
      fetch();
    }
  }, [attributeSlug, deviceSlug]);

  return (
    <Spinner loading={!!!attribute}>
      {current?.id === devices.find((d) => d.slug === deviceSlug)?.userId && <ImportDataModal />}

      <Row style={{ width: '1200px', margin: 'auto' }} justify="space-between">
        {attribute?.dataType !== AttributeDataType.STRING && (
          <>
            <Col span={12}>
              <FrequentiyChart attribute={attribute!} />
            </Col>
            <Col span={11}>
              <div style={{ marginBottom: 12 }}>Realtime:</div>
              <RealtimeChart attribute={attribute} />
            </Col>
            <Divider />
          </>
        )}
        {attribute && attribute?.dataType !== AttributeDataType.STRING && (
          <Col span={24}>
            <OldDataChart attribute={attribute} />

            <Divider />
          </Col>
        )}
        <Col span={24}>
          <div style={{ marginTop: 24 }}>{attribute && <List attribute={attribute} />}</div>
        </Col>
      </Row>
    </Spinner>
  );
};

export default Attribute;
