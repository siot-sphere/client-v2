import { Col, Row } from 'antd';
import React, { useState } from 'react';

import { Upload } from 'antd';
import { DeviceService, UploadMedia } from 'services';
import { useRecoilState } from 'recoil';
import { currentUser } from 'recoil-stores';
import { Device } from 'types';
import { PlusOutlined } from '@ant-design/icons';
import { ErrorMessage } from 'utils';

export interface AvatarProps {
  device: Device;
}

const Avatar: React.SFC<AvatarProps> = ({ device }) => {
  const [images, setImages] = useState<any[]>([{ uid: device.image, url: device.image, name: device.image }]);

  const handleChange = async ({ fileList }: any) => {
    setImages(fileList);

    try {
      if (fileList.length) {
        const data = await UploadMedia(fileList[0].originFileObj);

        const url = await data.ref.getDownloadURL();

        await DeviceService.editDeviceOwner(device.slug, { image: url });

        window.location.reload();
      }
    } catch (error) {
      ErrorMessage();
    }
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  const [user] = useRecoilState(currentUser);

  return (
    <div>
      <Row>
        <Col span={12}>
          <div style={{ marginTop: 24 }}>
            <Upload
              listType="picture-card"
              fileList={images}
              onChange={handleChange}
              beforeUpload={() => false}
              disabled={user?.id !== device.userId}
            >
              {images.length < 1 && uploadButton}
            </Upload>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Avatar;
