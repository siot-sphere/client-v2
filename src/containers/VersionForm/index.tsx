import { Button, Col, Form, message, Row, Typography } from 'antd';
import { AppInput, FormItem, Dropdown } from 'components';
import { AppRoute } from 'helpers';
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { PlatformVersionService } from 'services';
import { PlatformVersionEntity, PlatformVersionReleaseEnum } from 'types';
import { ErrorMessage } from 'utils';
import UploadFile from './UploadFile';
import ReleaseDescriptionInput from './ReleaseDescriptionInput';
import ReleaseKeyInput from './ReleaseKeyInput';
import { capitalize } from 'lodash';

const { Text } = Typography;

export interface VersionFormProps {
  platformVersion?: PlatformVersionEntity;
}

function VersionForm({ platformVersion }: VersionFormProps) {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false);

  const history = useHistory();

  const handleSubmit = async () => {
    form.validateFields().then(
      async () => {
        setLoading(true);
        const { name, description, releaseType, files, releaseKey } = form.getFieldsValue();
        try {
          if (platformVersion) {
            await PlatformVersionService.editPlatformVersionById(platformVersion.id!, {
              name,
              description,
              releaseType: releaseType[0],
              medias: files,
              releaseKey,
            });
          } else
            await PlatformVersionService.createNewPlatformVersion({
              description,
              name,
              releaseType: releaseType[0],
              medias: files,
              releaseKey,
            });

          message.success(platformVersion ? 'Edited Successfully!' : 'New Release Version created successfully!');
          history.push(AppRoute.versions);
        } catch (error) {
          ErrorMessage();
        }
        setLoading(false);
      },
      () => {},
    );
  };

  return (
    <Row className="w-full h-full flex-col">
      <Col>
        <div className="text-xl">{platformVersion ? 'Edit SIoT Platform Version' : 'New SIoT Platform Version'}</div>
      </Col>
      <Col className="bg-white flex-grow mt-8 rounded shadow">
        <Form
          name="platform-version"
          form={form}
          initialValues={{
            ...platformVersion,
            releaseType: platformVersion?.releaseType ? [platformVersion.releaseType] : undefined,
          }}
        >
          <div className="p-8 px-12">
            <Row>
              <Col span={8} className="max-w-xs mt-2">
                <Text className="text-sm font-semibold">Version Name *</Text>
              </Col>
              <Col span={16} className="max-w-2xl">
                <div>
                  <FormItem name="name" rules={[{ required: true }]}>
                    <AppInput placeholder="Version Name" />
                  </FormItem>
                </div>
              </Col>
            </Row>
            <Row>
              <Col span={8} className="max-w-xs mt-2">
                <Text className="text-sm font-semibold">Release Key *</Text>
              </Col>
              <Col span={16} className="max-w-2xl">
                <div>
                  <ReleaseKeyInput disabled={!!platformVersion} platformVersion={platformVersion} />
                </div>
              </Col>
            </Row>
            <Row>
              <Col span={8} className="max-w-xs mt-2">
                <Text className="text-sm font-semibold">Release Type *</Text>
              </Col>
              <Col span={16} className="max-w-2xl">
                <div>
                  <Form.Item name="releaseType" rules={[{ required: true }]}>
                    <Dropdown
                      placeholder="Select release type"
                      options={Object.keys(PlatformVersionReleaseEnum).map((type) => ({
                        code: type,
                        name: capitalize(type),
                      }))}
                      className="text-sm"
                    />
                  </Form.Item>
                </div>
              </Col>
            </Row>
            <Row>
              <Col span={8} className="max-w-xs mt-2">
                <Text className="text-sm font-semibold">Media file *</Text>
              </Col>
              <Col span={16} className="max-w-2xl">
                <div>
                  <Form.Item name="files" rules={[{ required: true }]}>
                    <UploadFile form={form} defaultFileList={platformVersion?.medias || []} />
                  </Form.Item>
                </div>
              </Col>
            </Row>
            <Row>
              <Col span={8} className="max-w-xs mt-2">
                <Text className="text-sm font-semibold">Description *</Text>
              </Col>
              <Col span={16} className="max-w-2xl">
                <div>
                  <Form.Item name="description" rules={[{ required: true }]}>
                    <ReleaseDescriptionInput form={form} initValue={platformVersion?.description} />
                  </Form.Item>
                </div>
              </Col>
            </Row>
          </div>
        </Form>
      </Col>
      <Col className="mt-2">
        <Row justify="end">
          <Button type="primary" onClick={handleSubmit} loading={loading} disabled={loading}>
            Save Release Version
          </Button>
        </Row>
      </Col>
    </Row>
  );
}

export default VersionForm;
