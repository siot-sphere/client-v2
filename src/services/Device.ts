import superagent from 'superagent';
import SuperagentPrefix from 'superagent-prefix';
import { Device, DeviceEditInput, Attribute, AttributeValue } from 'types';

const prefix = SuperagentPrefix(process.env.REACT_APP_API_ENDPOINT + '/api/devices');

async function save({ name, image, slug, attributes }: Device) {
  const token = localStorage.getItem('accessToken');

  return superagent
    .post('/')
    .use(prefix)
    .set('Authorization', `Bearer ${token}`)
    .send({ name, image, slug, attributes }) as unknown as Promise<{ body: Device }>;
}

async function getDeviceByUser() {
  const token = localStorage.getItem('accessToken');

  return superagent.get('/').use(prefix).set('Authorization', `Bearer ${token}`) as unknown as Promise<{
    body: Device[];
  }>;
}

async function getDeviceBySlugName(slug: string) {
  const token = localStorage.getItem('accessToken');

  return superagent
    .get('/' + slug)
    .use(prefix)
    .set('Authorization', `Bearer ${token}`) as unknown as Promise<{ body: Device }>;
}

async function editDeviceOwner(slug: string, input: DeviceEditInput) {
  const token = localStorage.getItem('accessToken');

  return superagent
    .post('/' + slug)
    .use(prefix)
    .set('Authorization', `Bearer ${token}`)
    .send(input) as unknown as Promise<{ body: Device }>;
}

async function insert(deviceSlug: string, attributeSlug: string, value: any) {
  const token = localStorage.getItem('accessToken');

  return await superagent
    .post('/' + deviceSlug + '/attributes/' + attributeSlug)
    .use(prefix)
    .set('Authorization', 'Bearer ' + token)
    .send({ value });
}

async function getFrequentlyAttributeValue(deviceSlug: string, attributeSlug: string, from: string, to: string) {
  const token = localStorage.getItem('accessToken');

  return (await superagent
    .get('/' + deviceSlug + '/attributes/' + attributeSlug + '/frequently?from=' + from + '&to=' + to)
    .use(prefix)
    .set('Authorization', 'Bearer ' + token)) as unknown as Promise<{
    body: AttributeValue[];
  }>;
}

async function getAttributeValueFilter(
  deviceSlug: string,
  attributeSlug: string,
  { from, to }: { from: string; to: string },
) {
  const token = localStorage.getItem('accessToken');

  return (await superagent
    .get('/' + deviceSlug + '/attributes/' + attributeSlug + '/filter?from=' + from + '&to=' + to)
    .use(prefix)
    .set('Authorization', 'Bearer ' + token)) as unknown as Promise<{
    body: AttributeValue[];
  }>;
}

async function getAttributeValuePaging(
  deviceSlug: string,
  attributeSlug: string,
  { page, from, to }: { page: number; from: string; to: string },
) {
  const token = localStorage.getItem('accessToken');

  return (await superagent
    .get('/' + deviceSlug + '/attributes/' + attributeSlug + '/paging?page=' + page + '&from=' + from + '&to=' + to)
    .use(prefix)
    .send({ page, from, to })
    .set('Authorization', 'Bearer ' + token)) as unknown as Promise<{ body: [AttributeValue[], number] }>;
}

async function getAttributeDetail(name: string) {
  const token = localStorage.getItem('accessToken');

  return (await superagent
    .get(process.env.REACT_APP_API_ENDPOINT + '/api/attributes/' + name)
    .set('Authorization', 'Bearer ' + token)) as unknown as Promise<{
    body: Attribute;
  }>;
}

async function updateAttribute(attribute: Attribute) {
  const token = localStorage.getItem('accessToken');

  return (await superagent
    .post(process.env.REACT_APP_API_ENDPOINT + '/api/attributes/' + attribute.id)
    .send(attribute)
    .set('Authorization', 'Bearer ' + token)) as unknown as Promise<{
    body: Attribute;
  }>;
}

async function newAttribute(device: string, { name, slug, dataLabel, dataType }: Attribute) {
  const token = localStorage.getItem('accessToken');

  return (await superagent
    .post('/' + device + '/new')
    .use(prefix)
    .send({ name, slug, dataLabel, dataType })
    .set('Authorization', 'Bearer ' + token)) as unknown as Promise<{
    body: Attribute;
  }>;
}

async function deleteAttribute(attributeId: number) {
  const token = localStorage.getItem('accessToken');

  return await superagent
    .delete(process.env.REACT_APP_API_ENDPOINT + '/api/attributes/' + attributeId)
    .set('Authorization', 'Bearer ' + token);
}

async function saveOldDeviceValues(
  deviceSlug: string,
  attributeSlug: string,
  data: { value: any; createdAt: string }[],
) {
  const token = localStorage.getItem('accessToken');

  return await superagent
    .post('/' + deviceSlug + '/attributes/' + attributeSlug + '/old-datas')
    .use(prefix)
    .set('Authorization', 'Bearer ' + token)
    .send(data);
}

async function deleteDevice(slug: string) {
  const token = localStorage.getItem('accessToken');

  return await superagent
    .delete('/' + slug)
    .use(prefix)
    .set('Authorization', 'Bearer ' + token);
}

export default {
  save,
  getDeviceByUser,
  getDeviceBySlugName,
  editDeviceOwner,
  insert,
  getFrequentlyAttributeValue,
  getAttributeValuePaging,
  getAttributeDetail,
  updateAttribute,
  deleteAttribute,
  newAttribute,
  saveOldDeviceValues,
  getAttributeValueFilter,
  deleteDevice,
};
