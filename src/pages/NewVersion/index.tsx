import React from 'react';
import { VersionFormContainer } from 'containers';

export interface Props {}

function NewPlatformVersion(props: Props) {
  return <VersionFormContainer />;
}

export default NewPlatformVersion;
