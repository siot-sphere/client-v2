import superagent from 'superagent';
import SuperagentPrefix from 'superagent-prefix';

const prefix = SuperagentPrefix(process.env.REACT_APP_API_ENDPOINT + '/api/slug');

async function suggest({ name, type }: { name: string; type: 'DEVICE' | 'ATTRIBUTE' }) {
  const token = localStorage.getItem('accessToken');

  return superagent
    .post('/suggest')
    .use(prefix)
    .set('Authorization', `Bearer ${token}`)
    .send({ name, type }) as unknown as Promise<{ body: { slug: string } }>;
}

async function verify(name: string) {
  const token = localStorage.getItem('accessToken');

  return superagent
    .post('/verify')
    .use(prefix)
    .set('Authorization', `Bearer ${token}`)
    .send({ name }) as unknown as Promise<{ body: { verify: boolean } }>;
}

export default { suggest, verify };
