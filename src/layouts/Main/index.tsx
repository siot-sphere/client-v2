import React, { ReactNode, useState } from 'react';
import ProLayout from '@ant-design/pro-layout';
import defaultProps from './_defaultProps';
import { Link } from 'react-router-dom';
import Header from './Header';

import './style.less';
import Logo from './Logo';

interface LayoutProps {
  children: ReactNode;
  collapsedable?: boolean;
}

export default function MainLayout({ children, collapsedable = true }: LayoutProps) {
  const [collapsed, setCollapsed] = useState(collapsedable ? false : true);

  return (
    <ProLayout
      {...defaultProps}
      fixedHeader={true}
      fixSiderbar={true}
      collapsed={collapsed}
      onCollapse={(collapsed) => collapsedable && setCollapsed(collapsed)}
      menuHeaderRender={() => <Logo collapsed={collapsed} />}
      menuItemRender={(item, dom) => <Link to={item.path!}>{dom}</Link>}
      navTheme="light"
      headerContentRender={() => <Header />}
      siderWidth={250}
      hasSiderMenu={false}
      contentStyle={{ margin: collapsedable ? undefined : 0 }}
      collapsedButtonRender={collapsedable ? undefined : false}
    >
      <div className="flex-grow">{children}</div>
    </ProLayout>
  );
}
