import React from 'react';
import { Attribute, Device } from 'types';
import List from './AttributeList';

export interface AttributesProps {
  attributes: Attribute[];
  device?: Device;
}

const Attributes: React.SFC<AttributesProps> = ({ attributes, device }) => {
  return <List attributes={attributes} device={device} />;
};

export default Attributes;
